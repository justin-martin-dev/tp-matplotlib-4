import random
import numpy as np

x = range(10)
y = [random.randint(0, 10) for i in x]

random_array_use_pie_chart = np.random.multinomial(100, np.ones(4)/4, size=1)[0]

print(x)
print(y)
print(random_array_use_pie_chart)
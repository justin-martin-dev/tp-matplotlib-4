import random

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')

random_array_use_hist_chart = [random.randint(0, 100) for i in range(50)]
random_array_use_pie_chart = np.random.multinomial(100, np.ones(4)/4, size=1)[0]

plt.title("Hist chart")
plt.grid(True)
plt.hist(random_array_use_hist_chart)
plt.legend()
plt.show()

plt.title("Pie chart")
plt.pie(random_array_use_pie_chart)
plt.axis('equal')
plt.show()

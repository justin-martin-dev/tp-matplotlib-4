import matplotlib.pyplot as plt
import numpy as np

x = np.arange(-5, 5, 0.01)
y = np.arange(-10, 28, 0.4)
X, Y = np.meshgrid(x, y)
Z = X*Y

fig = plt.figure()
ax1 = fig.add_subplot(projection='3d')
ax1.plot_wireframe(X, Y, Z)
ax1.set_title("2D in 3D")
plt.show()
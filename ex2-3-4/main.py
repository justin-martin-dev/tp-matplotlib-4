import random
import matplotlib.pyplot as plt
plt.style.use('ggplot')

values1 = range(10)
values2 = [random.randint(0, 10) for i in values1]

plt.title("Random value graph (2-3-4)")
plt.grid(True)
plt.plot(values1, "blue", label="Value 1")
plt.plot(values2, "green", linestyle='dashed', label="Value 2")

plt.annotate('Arrow', xy=(35, 50), xytext=(50, 5.5), arrowprops={'facecolor': 'black'})
plt.legend()
plt.show()